<%@ page import="finals.Student" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: daman
  Date: 15.11.2020
  Time: 19:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp"%>
<html>
<head>
    <title>Profile</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet">
</head>
<body>
<%

    if (session.getAttribute("student")!=null){
        Student student = (Student) session.getAttribute("profile");


%>

<div class="card" style="width: 500px;height: 500px; margin: 10px auto; border: solid 1px dodgerblue;text-align: center">
    <img src="https://thevoicefinder.com/wp-content/themes/the-voice-finder/images/default-img.png" alt="Profile" style="display: block; margin-left: auto; margin-right: auto; width: 35%; height: 40%; padding: 0">
    <h1> <%=student.getF_name()%> </h1>
    <h1> <%=student.getL_name()%> </h1>
    <p class="title" style="color: gray; font-size: 18px;"><%=student.getGroup()%></p>
    <p><%=student.getYear()%></p>
    <p><%=student.getMajor()%></p>
</div>

<fieldset style="width: 500px; margin: 10px auto">
    <legend><h2>Information</h2></legend>
    <h3>First Visit: </h3> <%=new Date(session.getCreationTime())%>
    <h3>Last Visit: </h3><%=new Date(session.getLastAccessedTime())%>
</fieldset>
<%
    }
%>
</body>
</html>
