<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Welcome</title>
    <link href="${request.getContextPath}/css/style.css" rel="stylesheet">
    <style>
        .clubImg{
            width: 170px;
            height: 150px;
            border-radius: 8px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
        legend{
            color: dodgerblue;
        }
    </style>
    <script src=http://code.jquery.com/jquery-1.11.2.min.js ></script>
    <script type="text/javascript">
        $(document).ready(function() {
            //Add Event:
            $('#event_id').bind('blur',function () {
                var event_id = $('#event_id').val()
                $.ajax({
                    url : 'EventsServlet',
                    method:'POST',
                    dataType: "HTML",
                    data : {
                        checkEvent:event_id,
                    },
                    success : function(response) {
                        if(response=="not"){
                            $('#msgEvent').text("Event with such ID already exist");
                            $('#addEvent').hide();
                        }else {
                            $('#msgEvent').text("Event ID is available");
                            $('#addEvent').show();
                        }
                    }
                });
            });
            //Add News:
            $('#new_id').bind('blur',function () {
                var new_id = $('#new_id').val()
                $.ajax({
                    url : 'NewsServlet',
                    method:'POST',
                    dataType: "HTML",
                    data : {
                        checkNews:new_id,
                    },
                    success : function(response) {
                        if(response=="not"){
                            $('#msgNews').text("Such ID already exist");
                            $('#addNew').hide();
                        }else {
                            $('#msgNews').text("New ID is available");
                            $('#addNew').show();
                        }
                    }
                });
            });

            //Add Club:
            $('#club_id').bind('blur',function () {
                var club_id = $('#club_id').val()
                $.ajax({
                    url : 'ClubsServlet',
                    method:'POST',
                    dataType: "HTML",
                    data : {
                        checkClubs:club_id,
                    },
                    success : function(response) {
                        if(response=="not"){
                            $('#msgClubs').text("Such Club ID already exist");
                            $('#addClub').hide();
                        }else {
                            $('#msgClubs').text("Club ID is available");
                            $('#addClub').show();
                        }
                    }
                });
            });
        });
    </script>
</head>
<body>
<fieldset>
    <legend>EVENTS</legend>
    <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
        <c:forEach items="${requestScope.events}" var="event">

        <div class="itemDiv">
            <h3> <c:out value="${event.title}"/> </h3>
            <h5>Date: <c:out value="${event.publ_date}"/></h5>
            <p> <c:out value="${event.body}"/></p>
        </div>
            <c:if test="${sessionScope.admin != null}">
                <form method="post" action="${request.getContextPath}EventsServlet">
                    <input type="text" name="event_id" readonly value="<c:out value="${event.event_id}"/>"><br>
                    <input type="text" name="event_date" value="<c:out value="${event.publ_date}"/>"><br>
                    <input type="text" name="event_title" value="<c:out value="${event.title}"/>"><br>
                    <textarea rows="6" cols="50" type="text" name="event_body"><c:out value="${event.body}"/></textarea><br>

                    <input type="submit" name="updateEvent" value="Edit"><br>
                    <input type="submit" name="deleteEvent" value="Delete">
                </form>
            </c:if>
        </c:forEach>
        <c:if test="${sessionScope.admin != null}">
    </div>
    <br><br>
    <form method="post" action="${request.getContextPath}EventsServlet">
        <legend>Add New Event</legend>
        <input type="text" id="event_id" name="event_id" placeholder="Event ID"><br><br>
        <input type="text" id="event_date" name="event_date" placeholder="Publish Date"><br><br>
        <input type="text" id="event_title" name="event_title" placeholder="Event Title"><br><br>
        <textarea rows="6" cols="50" id="event_body" name="event_body" placeholder="Event Body"></textarea><br><br>
        <input type="submit" name="addEvent" value="Add New Event" id="addEvent">
    </form>
    <p id="msgEvent"></p>
        </c:if>
</fieldset>

<fieldset>
    <legend>NEWS</legend>
    <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
        <c:forEach items="${requestScope.news}" var="newN">
        <div class="itemDiv">
            <h3> <c:out value="${newN.title}"/> </h3>
            <p> <c:out value="${newN.body}"/> </p>
        </div>
            <c:if test="${sessionScope.admin != null}">
                <form method="post" action="${request.getContextPath}EventsServlet">
                    <input type="text" name="new_id" readonly value="<c:out value="${newN.new_id}"/>"><br>
                    <input type="text" name="new_title" value="<c:out value="${newN.title}"/>"><br>
                    <textarea rows="5" cols="40" name="new_body"><c:out value="${newN.body}"/></textarea><br>

                    <input type="submit" name="updateNew" value="Edit"><br>
                    <input type="submit" name="deleteNew" value="Delete">
                </form>
            </c:if>
        </c:forEach>
            <c:if test="${sessionScope.admin != null}">
    </div>
    <br><br>
    <form method="post" action="${request.getContextPath}EventsServlet">
        <LEGEND>Add News</LEGEND>
        <input type="text" id="new_id" name="new_id" placeholder="ID"><br><br>
        <input type="text" id="new_title" name="new_title" placeholder="New's Title"><br><br>
        <textarea rows="5" cols="40" id="new_body" name="new_body" placeholder="New's Body"></textarea><br><br>
        <input type="submit" name="addNew" value="Add News" id="addNew">
    </form>
    <p id="msgNews"></p>
            </c:if>
</fieldset>

<fieldset>
    <legend>CLUBS</legend>
    <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
    <c:forEach items="${requestScope.clubs}" var="club">
        <div class="itemDiv">
            <img class="clubImg" src="<c:out value="${club.image}"/>" alt="ClubImage">
            <h3> <c:out value="${club.club_name}"/> </h3>
            <p> <c:out value="${club.club_description}"/> </p>
        </div>

        <c:if test="${sessionScope.admin != null}">
            <form method="post" action="${request.getContextPath}EventsServlet">
                <input type="text" name="club_id" readonly value="<c:out value="${club.club_id}"/>"><br>
                <input type="text" name="club_name" value="<c:out value="${club.club_name}"/>"><br>
                <textarea name="club_description" rows="5" cols="50"><c:out value="${club.club_description}"/></textarea><br>
                <input type="text" name="club_image" value="<c:out value="${club.image}"/>"><br>

                <input type="submit" name="updateClub" value="Edit"><br>
                <input type="submit" name="deleteClub" value="Delete">
            </form>
        </c:if>
    </c:forEach>
        <c:if test="${sessionScope.admin != null}">
    </div>
    <br><br>
    <form method="post" action="${request.getContextPath}EventsServlet">
        <legend>Add New Club</legend>
        <input type="text" id="club_id" name="club_id" placeholder="CLUB ID"><br><br>
        <input type="text" id="club_name" name="club_name" placeholder="Club's Name"><br><br>
        <textarea id="club_description" name="club_description" placeholder="DESCRIPTION" rows="5" cols="50"></textarea><br><br>
        <input type="text" id="club_image" name="club_image" placeholder="Club Image"><br><br>
        <input type="submit" name="addClub" value="Add Clubs" id="addClub">
    </form>
    <p id="msgClubs"></p>
        </c:if>
</fieldset>

</body>
</html>
