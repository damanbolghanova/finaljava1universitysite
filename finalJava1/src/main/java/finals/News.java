package finals;

public class News {
    private String new_id;
    private String title;
    private String body;

    public News(String new_id, String title, String body) {
        this.new_id = new_id;
        this.title = title;
        this.body = body;
    }

    public News() {
    }

    public String getNew_id() {
        return new_id;
    }

    public void setNew_id(String new_id) {
        this.new_id = new_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
