package finals;

public class Student {
    private String student_id;
    private String f_name;
    private String l_name;
    private String password;
    private String group;
    private String major;
    private String year;

    public Student(String student_id, String f_name, String l_name, String group, String major, String year,String password) {
        this.student_id = student_id;
        this.f_name = f_name;
        this.l_name = l_name;
        this.group = group;
        this.major = major;
        this.year = year;
        this.password = password;
    }

    public Student(String[] studentFields){
        if (studentFields.length == 7){
            this.student_id = studentFields[0];
            this.f_name = studentFields[1];
            this.l_name = studentFields[2];
            this.group = studentFields[3];
            this.major = studentFields[4];
            this.year = studentFields[5];
            this.password = studentFields[6];
        }
    }

    public Student() {
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
