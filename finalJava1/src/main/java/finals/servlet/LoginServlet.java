package finals.servlet;

import finals.Student;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        //Login
        String id = request.getParameter("id");
        String password = request.getParameter("password");

        if (id.equals("admin") && password.equals("1")) {
            session.setAttribute("admin", id);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else{
            try {
                Student student = DB.checkStudent(id, password);
                System.out.println(student.getF_name());
                session.setAttribute("student", student.getF_name());
                session.setAttribute("profile", student);
                request.getRequestDispatcher("index.jsp").forward(request, response);
            } catch (SQLException | NamingException e) {
                e.printStackTrace();
            }

        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
