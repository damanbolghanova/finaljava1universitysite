package finals.servlet;

import finals.Clubs;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@WebServlet(name = "ClubsServlet")
public class ClubsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("addClub")!=null){
            String club_id = request.getParameter("club_id");
            String club_name = request.getParameter("club_name");
            String club_description = request.getParameter("club_description");
            String club_image = request.getParameter("club_image");

            Clubs clubs = new Clubs(club_id, club_name, club_description, club_image);
            try{
                DB.insertClubs(clubs);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("update")!=null){
            String club_id = request.getParameter("club_id");
            String club_name = request.getParameter("club_name");
            String club_description = request.getParameter("club_description");
            String club_image = request.getParameter("club_image");

            Clubs clubs = new Clubs(club_id, club_name, club_description, club_image);
            try{
                DB.updateClubs(clubs);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("delete")!=null){
            String club_id = request.getParameter("club_id");
            try{
                DB.deleteClubs(club_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the Club ID exists or not:
        if (request.getParameter("checkClubs")!=null){
            try {
                String  sr;
                if (DB.getClubById(request.getParameter("checkClubs"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
