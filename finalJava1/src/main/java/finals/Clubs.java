package finals;

public class Clubs {
    private String club_id;
    private String club_name;
    private String club_description;
    private String image;

    public Clubs(String club_id, String club_name, String club_description, String image) {
        this.club_id = club_id;
        this.club_name = club_name;
        this.club_description = club_description;
        this.image = image;
    }

    public Clubs(){}
    public String getClub_id() {
        return club_id;
    }

    public void setClub_id(String club_id) {
        this.club_id = club_id;
    }

    public String getClub_name() {
        return club_name;
    }

    public void setClub_name(String club_name) {
        this.club_name = club_name;
    }

    public String getClub_description() {
        return club_description;
    }

    public void setClub_description(String club_description) {
        this.club_description = club_description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
