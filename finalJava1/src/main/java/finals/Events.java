package finals;

import java.util.Date;

public class Events {
     private String event_id;
     private String publ_date;
     private String title;
     private String body;

    public Events(String event_id, String publ_date, String title, String body) {
        this.event_id = event_id;
        this.publ_date = publ_date;
        this.title = title;
        this.body = body;
    }

    public Events() {
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getPubl_date() {
        return publ_date;
    }

    public void setPubl_date(String publ_date) {
        this.publ_date = publ_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
