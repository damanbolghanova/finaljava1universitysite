package finals.servlet;

import finals.Events;
import finals.News;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@WebServlet(name = "NewsServlet")
public class NewsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("addNew")!=null){
            String new_id = request.getParameter("new_id");
            String new_title = request.getParameter("new_title");
            String new_body = request.getParameter("new_body");

            News news = new News(new_id, new_title, new_body);
            try{
                DB.insertNews(news);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("update")!=null){
            String new_id = request.getParameter("new_id");
            String new_title = request.getParameter("new_title");
            String new_body = request.getParameter("new_body");

            News news = new News(new_id, new_title, new_body);
            try{
                DB.updateNews(news);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("delete")!=null){
            String new_id = request.getParameter("new_id");
            try{
                DB.deleteNews(new_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the New's ID exists or not:
        if (request.getParameter("checkNews")!=null){
            try {
                String  sr;
                if (DB.getNewsById(request.getParameter("checkNews"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
