<%@ page import="finals.database.DB" %>
<%@ page import="finals.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Students</title>
    <link href="${request.getContextPath}css/style.css" rel="stylesheet">
    <script src=http://code.jquery.com/jquery-1.11.2.min.js ></script>
    <script>
        $(document).ready(function() {
            //Register Student:
            $('#Id').bind('blur', function () {
                var student_id = $('#Id').val()
                $.ajax({
                    url: 'StudentsServlet',
                    method: 'POST',
                    dataType: "HTML",
                    data: {
                        checkStudent: student_id,
                    },
                    success: function (response) {
                        if (response == "not") {
                            $('#msg').text("Student with such ID already exist");
                            $('#submit').hide();
                        } else {
                            $('#msg').text("Student ID is available");
                            $('#submit').show();
                        }
                    }
                });
            });
        });
        function myFunction1() {
            document.getElementById("myDropdown1").classList.toggle("show");
        }

        function myFunction2() {
            document.getElementById("myDropdown2").classList.toggle("show");
        }
    </script>
</head>
<body>


<form method="post" action="${request.getContextPath}StudentsServlet">
    <div class="studentsFound">
        <input type="text" style="width: 50%; height: 30px; margin: auto" placeholder="Student's First or Last Name or Group or Major" name="searchInput" id="searchInput">
        <input type="submit"  style="width: 15%;" name="search" value="Search">
        <input type="submit"  style="width: 15%;" name="allStudents" value="All Students"><br>
        <table width="700px" align="center"style="border:1px solid #000000;">
            <tr>
                <td colspan=5 align="center"
                    style="background-color:teal">
                    <b>Student Record</b></td>
            </tr>
            <tr style="background-color:lightgrey;">
                <td><b>First Name</b></td>
                <td><b>Last Name</b></td>
                <td><b>Group</b></td>
                <td><b>Major</b></td>
                <td><b>Year</b></td>
            </tr>
            <%
                for (Student studentsFound : DB.search(request.getParameter("searchInput"))){
            %>
            <tr>
                <td><%=studentsFound.getF_name()%></td>
                <td><%=studentsFound.getL_name()%></td>
                <td><%=studentsFound.getGroup()%></td>
                <td><%=studentsFound.getMajor()%></td>
                <td><%=studentsFound.getYear()%></td>
            </tr>
            <%
                }
            %>
            <%
                if (request.getParameter("allStudents")!=null){
                for (Student studentsFound : DB.getAllStudents()){
            %>
            <tr>
                <td><%=studentsFound.getF_name()%></td>
                <td><%=studentsFound.getL_name()%></td>
                <td><%=studentsFound.getGroup()%></td>
                <td><%=studentsFound.getMajor()%></td>
                <td><%=studentsFound.getYear()%></td>
            </tr>
            <%
                }}
            %>
        </table>
    </div>
</form>
<br><br>
    <div class="studentsFound">
    <div class="dropdown">
        <button onclick="myFunction1()" class="dropbtn">2022</button>
        <div id="myDropdown1" class="dropdown-content">
            <a href="${request.getContextPath}students.jsp?year=2022&major=Software+Engineering">Software Engineering</a>
            <a href="${request.getContextPath}students.jsp?year=2022&major=IT+Management">IT Management</a>
            <a href="${request.getContextPath}students.jsp?year=2022&major=Big+Data">Bid Data</a>
        </div>
    </div>

    <div class="dropdown">
        <button onclick="myFunction2()" class="dropbtn">2023</button>
        <div id="myDropdown2" class="dropdown-content">
            <a href="${request.getContextPath}students.jsp?year=2023&major=Software+Engineering">Software Engineering</a>
            <a href="${request.getContextPath}students.jsp?year=2023&major=IT+Management">IT Management</a>
            <a href="${request.getContextPath}students.jsp?year=2023&major=Big+Data">Bid Data</a>
        </div>
    </div>

<fieldset style="width: 80%; margin: 5px auto">
    <legend>Students</legend>
    <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
    <%
        String year = request.getParameter("year");
        String major = request.getParameter("major");
        for (Student student : DB.getStudentsByMajorYear(major, year)){
    %>
        <div class="itemDiv">
            <p><b>First Name: </b><%=student.getF_name()%></p>
            <p><b>Last Name: </b><%=student.getL_name()%></p>
            <p><b>Group: </b> <%=student.getGroup()%></p>
            <p><b>Major:</b> <%=student.getMajor()%></p>
            <p><b>Year: </b><%=student.getYear()%></p>
        </div>
    <%
        }
    %>
    </div>
</fieldset>
    </div>
    <c:if test="${sessionScope.admin != null}">
    <form action="${request.getContextPath}StudentsServlet" method="post" class="form">
        <h2 class="studentsFormH">Register New Student</h2>
        <p class="studentsFormP">
            <label for="Id" class="floatLabel">Id</label>
            <input class="studentsFormInput" id="Id" name="Id" type="text">
        </p>
        <p id="msg"></p>
        <p class="studentsFormP">
            <label for="FirstName" class="floatLabel">First Name</label>
            <input class="studentsFormInput" id="FirstName" name="FirstName" type="text">
        </p>
        <p class="studentsFormP">
            <label for="LastName" class="floatLabel">Last Name</label>
            <input class="studentsFormInput" id="LastName" name="LastName" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Group" class="floatLabel">Group</label>
            <input class="studentsFormInput" id="Group" name="Group" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Major" class="floatLabel">Major</label>
            <input class="studentsFormInput" id="Major" name="Major" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Year" class="floatLabel">Year</label>
            <input class="studentsFormInput" id="Year" name="Year" type="text">
        </p>
        <p class="studentsFormP">
            <label for="pass" class="floatLabel">Password</label>
            <input class="studentsFormInput" id="pass" name="Password" type="password">
        </p>
        <p class="studentsFormP">
            <input class="studentsFormInputSubmit" type="submit" name="register" value="Register Student" id="submit">
        </p>

    </form>
    </c:if>
</body>
</html>
