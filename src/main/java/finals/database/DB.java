package finals.database;

import finals.Clubs;
import finals.Events;
import finals.News;
import finals.Student;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

public class DB {
    public static Connection connection() throws SQLException, NamingException {
        Context initContext = new InitialContext();
        Context envContext  = (Context)initContext.lookup("java:/comp/env");
        DataSource ds = (DataSource)envContext.lookup("jdbc/week8");
        Connection conn = ds.getConnection();
        return conn;
    }

    //STUDENTS:
    public static String insertStudent(Student student) throws SQLException, NamingException{
        String sql = ("INSERT INTO `students`(`student_id`, `f_name`, `l_name`, `stud_group`, `major`, `year`, `password`) VALUES(?,?,?,?,?,?,?)");
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, student.getStudent_id());
        ps.setString(2, student.getF_name());
        ps.setString(3, student.getL_name());
        ps.setString(4, student.getGroup());
        ps.setString(5, student.getMajor());
        ps.setString(6, student.getYear());
        ps.setString(7, student.getPassword());

        ps.executeUpdate();
        ps.close();
        return  null;
    }

    public static Student checkStudent(String student_id, String password) throws SQLException, NamingException {
        String sql = ("select * from students where  student_id=? and password=?");
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, student_id);
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            Student student = new Student();
            student.setStudent_id(rs.getString(1));
            student.setF_name(rs.getString(2));
            student.setL_name(rs.getString(3));
            student.setGroup(rs.getString(4));
            student.setMajor(rs.getString(5));
            student.setYear(rs.getString(6));
            student.setPassword(rs.getString(7));
            return student;
        }
        rs.close();
        ps.close();
        return null;
    }

    public static LinkedList<Student> getStudentsByMajorYear(String major, String year) throws SQLException, NamingException{
        String sql = ("SELECT * FROM students WHERE major = ? AND year = ?");
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, major);
        ps.setString(2, year);
        ResultSet rs = ps.executeQuery();
        LinkedList<Student> list = new LinkedList<>();

        while (rs.next()){
            Student student = new Student();
            student.setStudent_id(rs.getString(1));
            student.setF_name(rs.getString(2));
            student.setL_name(rs.getString(3));
            student.setGroup(rs.getString(4));
            student.setMajor(rs.getString(5));
            student.setYear(rs.getString(6));
            student.setPassword(rs.getString(7));

            list.add(student);
        }
        rs.close();
        ps.close();
        return list;
    }

    public static Student getStudentById(String student_id) throws SQLException, NamingException{
        String sql = ("SELECT * FROM students WHERE student_id = ?");
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, student_id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            Student student = new Student();
            student.setStudent_id(rs.getString(1));
            student.setF_name(rs.getString(2));
            student.setL_name(rs.getString(3));
            student.setGroup(rs.getString(4));
            student.setMajor(rs.getString(5));
            student.setYear(rs.getString(6));
            student.setPassword(rs.getString(7));

            return student;
        }
        rs.close();
        ps.close();
        return null;
    }

    public static ArrayList<Student> getAllStudents() throws SQLException, NamingException{
        ArrayList<Student> list = new ArrayList<>();
        try
        {
            Statement statement = connection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM students");
            ResultSetMetaData metaData = resultSet.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            Student student;
            while (resultSet.next())
            {
                String[] studentFields = new String[numberOfColumns];
                for(int a=1; a<=numberOfColumns; a++)
                {
                    studentFields[a-1] = resultSet.getObject(a).toString();
                }
                student = new Student(studentFields);
                list.add(student);
            }
            resultSet.close();
            statement.close();
        }
        catch (SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        return list;
    }

    public static ArrayList<Student> search(String name)
    {
        ArrayList<Student> studentsList = new ArrayList();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try
        {
            connection = connection();
            preparedStatement = connection.prepareStatement("select * from students where f_name=? or l_name=? or stud_group=? or major=? or 'year'=?");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, name);
            preparedStatement.setString(4, name);
            preparedStatement.setString(5, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            ResultSetMetaData metaData = resultSet.getMetaData();
            int numberOfColumns = metaData.getColumnCount();
            Student student;
            while (resultSet.next())
            {
                String[] studentsField = new String[numberOfColumns];
                for(int a=1; a<=numberOfColumns; a++)
                {
                    studentsField[a-1] = resultSet.getObject(a).toString();
                }
                student = new Student(studentsField);
                studentsList.add(student);
            }
            resultSet.close();
            connection.close();
            preparedStatement.close();
        }
        catch (SQLException | NamingException e)
        {
            e.printStackTrace();
        }
        return studentsList;
    }

    // EVENTS:
    public static String insertEvent(Events event) throws SQLException, NamingException{
        String sql = "insert into events(event_id, publ_date, title, body) values(?,?,?,?)";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, event.getEvent_id());
        ps.setString(2, event.getPubl_date());
        ps.setString(3, event.getTitle());
        ps.setString(4, event.getBody());

        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static String updateEvent(Events event) throws SQLException, NamingException {
        String sql="update events set publ_date=?, title=?, body=? where event_id=?";

        PreparedStatement pst = connection().prepareStatement(sql);
        pst.setString(1,event.getPubl_date());
        pst.setString(2,event.getTitle());
        pst.setString(3,event.getBody());
        pst.setString(4,event.getEvent_id());
        pst.execute();

        pst.close();
        return null;
    }

    public static String deleteEvent(String event_id) throws SQLException, NamingException{
        String sql ="delete from events where event_id = ?";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, event_id);
        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static Stack<Events> getAllEvents() throws SQLException, NamingException{
        String sql = "select * from events";
        PreparedStatement ps = connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Stack<Events> list = new Stack<>();

        while (rs.next()){
            Events event = new Events();
            event.setEvent_id(rs.getString(1));
            event.setPubl_date(rs.getString(2));
            event.setTitle(rs.getString(3));
            event.setBody(rs.getString(4));

            list.add(event);
        }
        rs.close();
        ps.close();
        return list;
    }

    public static Events getEventById(String event_id) throws SQLException, NamingException{
        String sql = "select * from events where event_id = ?";
        PreparedStatement ps =connection().prepareStatement(sql);
        ps.setString(1, event_id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            Events event = new Events();
            event.setEvent_id(rs.getString(1));
            event.setPubl_date(rs.getString(2));
            event.setTitle(rs.getString(3));
            event.setBody(rs.getString(4));
            return event;
        }
        rs.close();
        ps.close();
        return null;
    }

    // NEWS:
    public static String insertNews(News news) throws SQLException, NamingException{
        String sql = "insert into news(new_id, title, body) values(?,?,?)";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, news.getNew_id());
        ps.setString(2, news.getTitle());
        ps.setString(3, news.getBody());

        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static String updateNews(News news) throws SQLException, NamingException {
        String sql="update news set title=?, body=? where new_id=?";

        PreparedStatement pst = connection().prepareStatement(sql);
        pst.setString(1,news.getTitle());
        pst.setString(2,news.getBody());
        pst.setString(3,news.getNew_id());
        pst.execute();
        pst.close();
        return null;
    }

    public static String deleteNews(String new_id) throws SQLException, NamingException{
        String sql ="delete from news where new_id = ?";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, new_id);
        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static ArrayList<News> getAllNews() throws SQLException, NamingException{
        String sql = "select * from news";
        PreparedStatement ps = connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<News> list = new ArrayList<>();

        while (rs.next()){
            News news = new News();
            news.setNew_id(rs.getString(1));
            news.setTitle(rs.getString(2));
            news.setBody(rs.getString(3));

            list.add(news);
        }
        rs.close();
        ps.close();
        return list;
    }

    public static News getNewsById(String new_id) throws SQLException, NamingException{
        String sql = "select * from news where new_id = ?";
        PreparedStatement ps =connection().prepareStatement(sql);
        ps.setString(1, new_id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            News news = new News();
            news.setNew_id(rs.getString(1));
            news.setTitle(rs.getString(2));
            news.setBody(rs.getString(3));
            return news;
        }
        rs.close();
        ps.close();
        return null;
    }


    // CLUBS:
    public static String insertClubs(Clubs clubs) throws SQLException, NamingException{
        String sql = "insert into clubs(club_id, club_name, club_description, image) values(?,?,?,?)";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, clubs.getClub_id());
        ps.setString(2, clubs.getClub_name());
        ps.setString(3, clubs.getClub_description());
        ps.setString(4, clubs.getImage());

        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static String updateClubs(Clubs clubs) throws SQLException, NamingException {
        String sql="update clubs set club_name=?, club_description=?, image=? where club_id=?";

        PreparedStatement pst = connection().prepareStatement(sql);
        pst.setString(1,clubs.getClub_name());
        pst.setString(2,clubs.getClub_description());
        pst.setString(3, clubs.getImage());
        pst.setString(4,clubs.getClub_id());
        pst.execute();
        pst.close();
        return null;
    }

    public static String deleteClubs(String club_id) throws SQLException, NamingException{
        String sql ="delete from clubs where club_id = ?";
        PreparedStatement ps = connection().prepareStatement(sql);
        ps.setString(1, club_id);
        ps.executeUpdate();
        ps.close();
        return null;
    }

    public static ArrayList<Clubs> getAllClubs() throws SQLException, NamingException{
        String sql = "select * from clubs";
        PreparedStatement ps = connection().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<Clubs> list = new ArrayList<>();

        while (rs.next()){
            Clubs clubs = new Clubs();
            clubs.setClub_id(rs.getString(1));
            clubs.setClub_name(rs.getString(2));
            clubs.setClub_description(rs.getString(3));
            clubs.setImage(rs.getString(4));

            list.add(clubs);
        }
        rs.close();
        ps.close();
        return list;
    }

    public static Clubs getClubById(String club_id) throws SQLException, NamingException{
        String sql = "select * from clubs where club_id = ?";
        PreparedStatement ps =connection().prepareStatement(sql);
        ps.setString(1, club_id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()){
            Clubs club = new Clubs();
            club.setClub_id(rs.getString(1));
            club.setClub_name(rs.getString(2));
            club.setClub_description(rs.getString(3));
            club.setImage(rs.getString(4));
            return club;
        }
        rs.close();
        ps.close();
        return null;
    }

}
