package finals.servlet;

import com.google.gson.Gson;
import finals.Student;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "StudentsServlet")
public class StudentsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("register")!=null){
            String id = request.getParameter("Id");
            String firstName = request.getParameter("FirstName");
            String lastName = request.getParameter("LastName");
            String group = request.getParameter("Group");
            String major = request.getParameter("Major");
            String year = request.getParameter("Year");
            String password = request.getParameter("Password");

            Student student = new Student(id, firstName, lastName, group, major, year, password);
            try{
                DB.insertStudent(student);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/students.jsp").forward(request, response);
        }

        //Check whether the Student ID exists or not:
        if (request.getParameter("checkStudent")!=null){
            try {
                String  sr;
                if (DB.getStudentById(request.getParameter("checkStudent"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException | NamingException e) {
                e.printStackTrace();
            }
        }
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Connection connection = DB.connection();
            ArrayList<Student> studentsList = DB.getAllStudents();
            connection.close();
            request.setAttribute("studentsList", studentsList);
        }
        catch (SQLException | NamingException e){
            e.printStackTrace();
        }
        request.getRequestDispatcher("students.jsp").forward(request,response);
    }
}