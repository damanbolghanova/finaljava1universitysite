<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet">
</head>
<body>

<%
    if(session.getAttribute("admin") == null && session.getAttribute("student") == null) {
        response.sendRedirect("login.jsp");
    }
%>

<header>
    <div class="header">
        <a href="#default" class="logo">University</a>
        <div class="header-right">
            <a class="active" href="<%=request.getContextPath()%>/index.jsp">Home</a>
            <a class="active" href="<%=request.getContextPath()%>/students.jsp">Students</a>
            <%
                if(session.getAttribute("admin") != null && session.getAttribute("student") != null) {
            %>
            <a class="active" href="<%=request.getContextPath()%>/login.jsp">Login</a><br>
            <%
                }
            %>

            <%
                if(session.getAttribute("admin") != null) {
            %>
            <h2>Welcome, <%=session.getAttribute("admin")%></h2>
            <%
                }
            %>

            <%
                if(session.getAttribute("student") != null) {
            %>
            <h2>Welcome, <%=session.getAttribute("student")%></h2>
            <%
                }
            %>

            <form method="post" action="<%=request.getContextPath()%>/LogoutServlet">
                <input type="submit" name="logOut" value="Logout">
            </form>

            <%
                if(session.getAttribute("student") != null) {
            %>
                    <form method="post" action="<%=request.getContextPath()%>/profile.jsp">
                        <input type="submit" name="profile" value="Profile">
                    </form>
            <%
                }
            %>
        </div>
    </div>
</header>

</body>
</html>
