package finals.servlet;

import finals.Clubs;
import finals.Events;
import finals.News;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Stack;

@WebServlet(name = "EventsServlet")
public class EventsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("addEvent")!=null){
            String event_date = request.getParameter("event_date");
            String event_id = request.getParameter("event_id");
            String event_title = request.getParameter("event_title");
            String event_body = request.getParameter("event_body");

            Events event = new Events(event_id, event_date, event_title, event_body);
            try{
                DB.insertEvent(event);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("updateEvent")!=null){
            String event_date = request.getParameter("event_date");
            String event_id = request.getParameter("event_id");
            String event_title = request.getParameter("event_title");
            String event_body = request.getParameter("event_body");

            Events event = new Events(event_id, event_date, event_title, event_body);
            try{
                DB.updateEvent(event);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("deleteEvent")!=null){
            String event_id = request.getParameter("event_id");
            try{
                DB.deleteEvent(event_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the Event ID exists or not:
        if (request.getParameter("checkEvent")!=null){
            try {
                String  sr;
                if (DB.getEventById(request.getParameter("checkEvent"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }

        if (request.getParameter("addNew")!=null){
            String new_id = request.getParameter("new_id");
            String new_title = request.getParameter("new_title");
            String new_body = request.getParameter("new_body");

            News news = new News(new_id, new_title, new_body);
            try{
                DB.insertNews(news);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("updateNew")!=null){
            String new_id = request.getParameter("new_id");
            String new_title = request.getParameter("new_title");
            String new_body = request.getParameter("new_body");

            News news = new News(new_id, new_title, new_body);
            try{
                DB.updateNews(news);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("deleteNew")!=null){
            String new_id = request.getParameter("new_id");
            try{
                DB.deleteNews(new_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the New's ID exists or not:
        if (request.getParameter("checkNews")!=null){
            try {
                String  sr;
                if (DB.getNewsById(request.getParameter("checkNews"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }


        if (request.getParameter("addClub")!=null){
            String club_id = request.getParameter("club_id");
            String club_name = request.getParameter("club_name");
            String club_description = request.getParameter("club_description");
            String club_image = request.getParameter("club_image");

            Clubs clubs = new Clubs(club_id, club_name, club_description, club_image);
            try{
                DB.insertClubs(clubs);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("updateClub")!=null){
            String club_id = request.getParameter("club_id");
            String club_name = request.getParameter("club_name");
            String club_description = request.getParameter("club_description");
            String club_image = request.getParameter("club_image");

            Clubs clubs = new Clubs(club_id, club_name, club_description, club_image);
            try{
                DB.updateClubs(clubs);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("deleteClub")!=null){
            String club_id = request.getParameter("club_id");
            try{
                DB.deleteClubs(club_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            doGet(request,response);
            //request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the Club ID exists or not:
        if (request.getParameter("checkClubs")!=null){
            try {
                String  sr;
                if (DB.getClubById(request.getParameter("checkClubs"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Connection connection = DB.connection();
            Stack<Events> events = DB.getAllEvents();
            connection.close();
            request.setAttribute("events", events);

        }catch (SQLException | NamingException e){
            e.printStackTrace();
        }

        try{
            Connection connection = DB.connection();
            ArrayList<News> news = DB.getAllNews();
            connection.close();
            request.setAttribute("news", news);

        }catch (SQLException | NamingException e){
            e.printStackTrace();
        }

        try{
            Connection connection = DB.connection();
            ArrayList<Clubs> clubs = DB.getAllClubs();
            connection.close();
            request.setAttribute("clubs", clubs);

        }catch (SQLException | NamingException e){
            e.printStackTrace();
        }

        request.getRequestDispatcher("index.jsp").forward(request,response);
    }
}
