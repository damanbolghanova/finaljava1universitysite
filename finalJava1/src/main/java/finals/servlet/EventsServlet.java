package finals.servlet;

import finals.Events;
import finals.database.DB;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Stack;

@WebServlet(name = "EventsServlet")
public class EventsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("addEvent")!=null){
            String event_date = request.getParameter("event_date");
            String event_id = request.getParameter("event_id");
            String event_title = request.getParameter("event_title");
            String event_body = request.getParameter("event_body");

            Events event = new Events(event_id, event_date, event_title, event_body);
            try{
                DB.insertEvent(event);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("updateEvent")!=null){
            String event_date = request.getParameter("event_date");
            String event_id = request.getParameter("event_id");
            String event_title = request.getParameter("event_title");
            String event_body = request.getParameter("event_body");

            Events event = new Events(event_id, event_date, event_title, event_body);
            try{
                DB.updateEvent(event);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        if (request.getParameter("deleteEvent")!=null){
            String event_id = request.getParameter("event_id");
            try{
                DB.deleteEvent(event_id);
            }catch (SQLException | NamingException e){
                e.printStackTrace();
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }

        //Check whether the Event ID exists or not:
        if (request.getParameter("checkEvent")!=null){
            try {
                String  sr;
                if (DB.getEventById(request.getParameter("checkEvent"))==null){
                    sr="available";
                }else {
                    sr="not";
                }
                response.setContentType("text/plain");
                OutputStream outStream = response.getOutputStream();
                outStream.write(sr.getBytes("UTF-8"));

                outStream.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Connection connection = DB.connection();
            Stack<Events> events = DB.getAllEvents();
            connection.close();
            request.setAttribute("events", events);

        }catch (SQLException | NamingException e){
            e.printStackTrace();
        }
        getServletContext().getRequestDispatcher("index.jsp").forward(request,response);
    }
}
