<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link href="${request.getContextPath}css/style.css" rel="stylesheet">
</head>
<body>
    <c:if test="${sessionScope.admin == null} && ${sessionScope.student == null}">
        <c:redirect url = "login.jsp"/>
    </c:if>
<header>
    <div class="header">
        <a href="#default" class="logo">University</a>
        <div class="header-right">
            <a class="active" href="${request.getContextPath}index.jsp">Home</a>
            <a class="active" href="${request.getContextPath}students.jsp">Students</a>

            <c:if test="${sessionScope.admin != null} && ${sessionScope.student != null}">
            <a class="active" href="${request.getContextPath}login.jsp">Login</a><br>
            </c:if>

            <c:if test="${sessionScope.admin != null}">
            <h2>Welcome, <%=session.getAttribute("admin")%></h2>
            </c:if>

            <c:if test="${sessionScope.student != null}">
            <h2>Welcome, <%=session.getAttribute("student")%></h2>
            </c:if>

            <form method="post" action="${request.getContextPath}LogoutServlet">
                <input type="submit" name="logOut" value="Logout">
            </form>

                <c:if test="${sessionScope.student != null}">
                    <form method="post" action="${request.getContextPath}profile.jsp">
                        <input type="submit" name="profile" value="Profile">
                    </form>
                </c:if>
        </div>
    </div>
</header>

</body>
</html>
