<%@ page import="finals.database.DB" %>
<%@ page import="finals.Student" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header.jsp"%>
<html>
<head>
    <title>Students</title>
    <link href="<%=request.getContextPath()%>/css/style.css" rel="stylesheet">
    <script src=http://code.jquery.com/jquery-1.11.2.min.js ></script>
    <script>
        function search()
        {
            var xhttp = new XMLHttpRequest();
            var name = document.getElementById("searchId").value;
            if (name == "")
            {
                document.getElementById("searchId").value = "please enter something";
                return;
            }
            xhttp.onreadystatechange = function ()
            {
                if (this.readyState == 4 && this.status == 200)
                {
                    var students = JSON.parse(this.responseText);
                    if(students.length>0)
                    {
                        //location.href = "#book-"+bookList[0].id;
                        document.getElementById("f_name").value = students[0].f_name;
                        document.getElementById("l_name").value = students[0].l_name;
                        document.getElementById("groupS").value = students[0].group;
                        document.getElementById("majorS").value = students[0].major;
                        document.getElementById("yearS").value = students[0].year;
                    }
                    else
                    {
                        document.getElementById("searchId").value = "Neither book nor movie with name "+name+" was found";
                    }
                }
            };
            xhttp.open("POST", "${pageContext.request.contextPath}/servlet", true);
            xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhttp.send("submit=search&name="+name);
        }
        $(document).ready(function() {
            //Register Student:
            $('#Id').bind('blur', function () {
                var student_id = $('#Id').val()
                $.ajax({
                    url: 'StudentsServlet',
                    method: 'GET',
                    dataType: "HTML",
                    data: {
                        checkStudent: student_id,
                    },
                    success: function (response) {
                        if (response == "not") {
                            $('#msg').text("Student with such ID already exist");
                            $('#submit').hide();
                        } else {
                            $('#msg').text("Student ID is available");
                            $('#submit').show();
                        }
                    }
                });
            });
        });
        function myFunction1() {
            document.getElementById("myDropdown1").classList.toggle("show");
        }

        function myFunction2() {
            document.getElementById("myDropdown2").classList.toggle("show");
        }
    </script>
</head>
<body>
<input type="text" id="searchId" style="width: 80%;" placeholder="Student's First or Last Name" name="search">
<input type="button" style="width: 15%;" name="search" value="search" onclick="search()">
<form method="post" action="<%=request.getContextPath()%>/StudentsServlet">
    <div class="itemDiv">
        <p id="f_name"><b>First Name: </b></p>
        <p id="l_name"><b>Last Name: </b></p>
        <p id="groupS"><b>Group: </b></p>
        <p id="majorS"><b>Major:</b></p>
        <p id="yearS"><b>Year: </b></p>
    </div>
</form>
    <div class="dropdown">
        <button onclick="myFunction1()" class="dropbtn">2022</button>
        <div id="myDropdown1" class="dropdown-content">
            <a href="<%=request.getContextPath()%>/students.jsp?year=2022&major=Software+Engineering">Software Engineering</a>
            <a href="<%=request.getContextPath()%>/students.jsp?year=2022&major=IT+Management">IT Management</a>
            <a href="<%=request.getContextPath()%>/students.jsp?year=2022&major=Big+Data">Bid Data</a>
        </div>
    </div>

    <div class="dropdown">
        <button onclick="myFunction2()" class="dropbtn">2023</button>
        <div id="myDropdown2" class="dropdown-content">
            <a href="<%=request.getContextPath()%>/students.jsp?year=2023&major=Software+Engineering">Software Engineering</a>
            <a href="<%=request.getContextPath()%>/students.jsp?year=2023&major=IT+Management">IT Management</a>
            <a href="<%=request.getContextPath()%>/students.jsp?year=2023&major=Big+Data">Bid Data</a>
        </div>
    </div>

<fieldset>
    <legend>Students</legend>
    <div style="display: grid;grid-template-columns: 1fr 1fr 1fr 1fr;">
    <%
        String year = request.getParameter("year");
        String major = request.getParameter("major");
        for (Student student : DB.getStudentsByMajorYear(major, year)){
    %>
        <div class="itemDiv">
            <p><b>First Name: </b><%=student.getF_name()%></p>
            <p><b>Last Name: </b><%=student.getL_name()%></p>
            <p><b>Group: </b> <%=student.getGroup()%></p>
            <p><b>Major:</b> <%=student.getMajor()%></p>
            <p><b>Year: </b><%=student.getYear()%></p>
        </div>
    <%
        }
    %>
    </div>
</fieldset>

    <%
        if (session.getAttribute("admin")!=null){
    %>
    <form action="<%=request.getContextPath()%>/StudentsServlet" method="post" class="form">
        <h2 class="studentsFormH">Register New Student</h2>
        <p class="studentsFormP">
            <label for="Id" class="floatLabel">Id</label>
            <input class="studentsFormInput" id="Id" name="Id" type="text">
        </p>
        <p id="msg"></p>
        <p class="studentsFormP">
            <label for="FirstName" class="floatLabel">First Name</label>
            <input class="studentsFormInput" id="FirstName" name="FirstName" type="text">
        </p>
        <p class="studentsFormP">
            <label for="LastName" class="floatLabel">Last Name</label>
            <input class="studentsFormInput" id="LastName" name="LastName" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Group" class="floatLabel">Group</label>
            <input class="studentsFormInput" id="Group" name="Group" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Major" class="floatLabel">Major</label>
            <input class="studentsFormInput" id="Major" name="Major" type="text">
        </p>
        <p class="studentsFormP">
            <label for="Year" class="floatLabel">Year</label>
            <input class="studentsFormInput" id="Year" name="Year" type="text">
        </p>
        <p class="studentsFormP">
            <label for="password" class="floatLabel">Password</label>
            <input class="studentsFormInput" id="password" name="Password" type="password">
        </p>
        <p class="studentsFormP">
            <input class="studentsFormInputSubmit" type="submit" name="register" value="Register Student" id="submit">
        </p>

    </form>

    <%
        }
    %>
</body>
</html>
