<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Login</title>
    <style>
        input[type = submit] {
            background-color: orange;
            text-decoration: none;
            color: white;
            padding: 10px 20px;
            cursor: pointer;
            width: 160px;
        }
        fieldset{
            background-color: #f1f1f1;
        }
    </style>
</head>
<body>
<div style="display: inline-grid; grid-template-columns: 1fr 1fr; grid-gap: 50px;margin-left: 550px; margin-top: 120px">
    <%-- Login part --%>
    <fieldset style="width: 150px;">
        <h3 style="color: black;">Authorization</h3>

        <form method="post" action="${request.getContextPath}LoginServlet">
            <input type="text" name="id" placeholder="ID"><br><br>
            <input type="password" name="password" placeholder="Password"><br><br>
            <input type="submit" name="logIn" value="Login">
        </form>
    </fieldset>
</div>
</body>
</html>
